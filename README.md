# jupyter-docker

Wraps Jupyter Notebook within a Docker container.
This container can then be used locally or with cloud offerings such as [binder](https://mybinder.org), [JupyterHub](http://jupyter.org/hub) and others.

Currently, this container offers:
*  [Jupyter Lab 3.0.5](https://jupyter.org)
*  Python 3, Jupyter Lab's default kernel
*  Java 11, Spencer Park's [IJava 1.3.0](https://github.com/SpencerPark/ijava) kernel

Holger Zahnleiter, 2021-02-14

## Use locally

The `Makefile` included herein demonstrates how to use the Docker image locally.
On the root of this project directory type:
1.  `make image` to build a Docker image
2.  `make run` to start a Docker container. Jupyter is accessible via web browser once the container has started
3.  `make shell` to open a shell into the running container. Meant for the purpose of debugging and experimenting

## Use with binder

1.  In your browser open the site [https://mybinder.org](https://mybinder.org)
2.  As a repository type select "GitLab.com"
3.  As repository url insert [https://gitlab.com/hzahnlei/jupyter-docker](https://gitlab.com/hzahnlei/jupyter-docker)
4.  For a branch or tag enter "master"
5.  Click on "launch" to start the Jupyter Notebook

Message to myself: For the binder badge to work with the latest versions of this image, do not forget to tag the Git repository.
In GitLab's badge configuraten update the tag, too.